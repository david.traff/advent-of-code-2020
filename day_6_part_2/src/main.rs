use std::collections::HashMap;
use std::fs::File;
use std::io::{prelude::*, BufReader, Result};

fn main() -> Result<()> {
    let file = File::open("./day_6_part_2/input")?;
    let reader = BufReader::new(file);

    let mut total = 0usize;

    let mut current_group = HashMap::new();
    let mut group_size = 0;
    for line in reader.lines() {
        let line = line.unwrap();

        if line.len() == 0 {
            for (_, count) in &current_group {
                if *count == group_size {
                    total += 1;
                }
            }
            current_group.clear();
            group_size = 0;
            continue;
        }

        group_size += 1;

        for c in line.chars() {
            *current_group.entry(c).or_insert(0) += 1;
        }
    }

    println!("Total is: {}", total);

    Ok(())
}
