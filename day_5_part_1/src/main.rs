use std::fs::File;
use std::io::{prelude::*, BufReader, Result};

fn main() -> Result<()> {
    let file = File::open("./day_5_part_1/input")?;
    let reader = BufReader::new(file);

    let mut max_id = 0;
    for line in reader.lines() {
        let line = line.unwrap();

        let mut front_back = BSPTree::new(128);
        let mut left_right = BSPTree::new(8);

        for c in line.chars() {
            match c {
                'F' => front_back.partition_lower(),
                'B' => front_back.partition_upper(),

                'L' => left_right.partition_lower(),
                'R' => left_right.partition_upper(),

                _ => unimplemented!(),
            }
        }

        let fb = front_back.get_result().unwrap();
        let lr = left_right.get_result().unwrap();

        let id = fb * 8 + lr;

        if id > max_id {
            max_id = id;
        }
    }

    println!("The highest id is: {}", max_id);

    Ok(())
}

#[derive(Debug)]
struct BSPTree {
    upper_bound: usize,
    lower_bound: usize,
}

impl BSPTree {
    pub fn new(size: usize) -> BSPTree {
        BSPTree {
            upper_bound: size - 1,
            lower_bound: 0,
        }
    }

    pub fn get_result(&self) -> Option<usize> {
        if self.upper_bound != self.lower_bound {
            return None;
        }

        Some(self.upper_bound)
    }

    pub fn partition_upper(&mut self) {
        let size = self.get_size();

        self.lower_bound += size / 2 + 1;
    }

    pub fn partition_lower(&mut self) {
        let size = self.get_size();

        self.upper_bound -= size / 2 + 1;
    }

    fn get_size(&self) -> usize {
        self.upper_bound - self.lower_bound
    }
}
