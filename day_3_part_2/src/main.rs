use std::fs::File;
use std::io::{prelude::*, BufReader, Result};

fn main() -> Result<()> {
    let slopes = vec![
        Slope { right: 1, down: 1 },
        Slope { right: 3, down: 1 },
        Slope { right: 5, down: 1 },
        Slope { right: 7, down: 1 },
        Slope { right: 1, down: 2 }
    ];
    let map = Map::new("./day_3_part_2/input")?;

    let mut total: u64 = 0;
    for slope in slopes {
        let mut trees = 0;
        let steps = map.height / slope.down;
        for i in 0..steps {
            if *map.get_point_at_step(i, &slope) {
                trees += 1;
            }
        }

        if total == 0 {
            total = trees;
        } else {
            total *= trees;
        }
    }

    println!("Total: {}", total);

    Ok(())
}

struct Slope {
    down: usize,
    right: usize,
}

struct Map {
    data: Vec<bool>,
    height: usize, // Height of data.
    width: usize,  // Stride of data.
}

impl Map {
    pub fn new(path: &str) -> Result<Map> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);
        let mut data = vec![];

        let mut width = 0usize;
        let mut height = 0usize;
        for line in reader.lines() {
            let line = line.unwrap();
            if width == 0 {
                width = line.len();
            }

            height += 1;

            for c in line.chars() {
                let tree = match c {
                    '#' => true,
                    '.' => false,
                    _ => panic!("Error parsing data."),
                };

                data.push(tree);
            }
        }

        Ok(Map {
            data,
            width,
            height,
        })
    }

    pub fn get_point_at_step(&self, step: usize, slope: &Slope) -> &bool {
        let x = (slope.right * step) % self.width;
        let y = slope.down * step;
        
        let idx = x + (y * self.width);

        &self.data[idx]
    }
}
