#![feature(str_split_once)]

use std::collections::HashMap;
use std::fs::File;
use std::io::{prelude::*, BufReader, Result};
use regex::Regex;

fn main() -> Result<()> {
    // muted lavender bags contain 5 dull brown bags, 4 pale maroon bags, 2 drab orange bags.
    let file = File::open("./day_7_part_2/input")?;
    let reader = BufReader::new(file);

    let child_regex = Regex::new(r"([0-9])\s(\w+\s\w+)").unwrap();

    let mut bags = HashMap::new();
    for line in reader.lines() {
        let line = line.unwrap();
        
        let (color, children) = line.split_once("bags contain").unwrap();
        let color = color.trim().to_string();

        let mut bag_children = vec![];

        for child in child_regex.captures_iter(children) {
            let amount = child[1].parse::<u32>().unwrap();
            let child_color = child[2].to_string();

            bags.entry(child_color.clone()).or_insert(vec![]);

            bag_children.push(Connection {
                name: child_color,
                amount,
            });
        }

        bags.insert(color, bag_children);
    }
    
    traverse(&bags, String::from("shiny gold"));

    Ok(())
}

fn traverse(data: &HashMap<String, Vec<Connection>>, name: String) {
    let connection = &Connection { name, amount: 1 };

    let total = traverse_parents(data, connection) - 1;

    println!("{}", total);
}

fn traverse_parents<'a>(data: &'a HashMap<String, Vec<Connection>>, connection: &'a Connection) -> u32 {
    let parents = data.get(&connection.name).unwrap();

    let mut total = connection.amount;

    for parent in parents {
        total += connection.amount * traverse_parents(data, parent);
    }
    
    total
}

#[derive(Debug)]
struct Connection {
    pub name: String,
    pub amount: u32,
}
