use std::fs::File;
use std::io::{prelude::*, BufReader, Result};

fn main() -> Result<()> {
    let file = File::open("./day_2_part_1/input")?;
    let reader = BufReader::new(file);

    let mut valid = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let parts: Vec<&str> = line.split(' ').collect();

        assert_eq!(parts.len(), 3);

        let (min, max) = get_min_max(parts[0]);
        let character = parts[1].chars().nth(0).unwrap();
        let password: Vec<char> = parts[2].chars().collect();

        if password[min] == character && password[max] != character {
            valid += 1;
        } else if password[min] != character && password[max] == character {
            valid += 1;
        }
    }

    println!("Valid passwords: {}", valid);

    Ok(())
}

fn get_min_max(data: &str) -> (usize, usize) {
    let delim = data.find('-').unwrap();

    let min = data[..delim].parse::<usize>().unwrap();
    let max = data[delim + 1..].parse::<usize>().unwrap();

    (min - 1, max - 1)
}
