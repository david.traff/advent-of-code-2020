use std::fs::File;
use std::io::{prelude::*, BufReader, Result};

fn main() -> Result<()> {
    let file = File::open("./day_2_part_1/input")?;
    let reader = BufReader::new(file);

    let mut valid = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let parts: Vec<&str> = line.split(' ').collect();

        assert_eq!(parts.len(), 3);

        let (min, max) = get_min_max(parts[0]);
        let character = parts[1].chars().nth(0).unwrap();
        let password = parts[2];

        let occurances = password.chars().filter(|c| *c == character).count();

        if occurances >= min && occurances <= max {
            valid += 1;
        }
    }

    println!("Valid password: {}", valid);

    Ok(())
}

fn get_min_max(data: &str) -> (usize, usize) {
    let delim = data.find('-').unwrap();

    let min = data[..delim].parse::<usize>().unwrap();
    let max = data[delim + 1..].parse::<usize>().unwrap();

    (min, max)
}
