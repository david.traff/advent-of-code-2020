use std::{fs::File};
use std::io::{prelude::*, BufReader, Result};
use std::collections::HashSet;

fn main() -> Result<()> {
    let file = File::open("./day_6_part_1/input")?;
    let reader = BufReader::new(file);

    let mut total = 0usize;
    let mut current_group = HashSet::with_capacity(26); // a-z = 26
    for line in reader.lines() {
        let line = line.unwrap();

        if line.len() == 0 {
            current_group.clear();
        }

        for c in line.chars() {
            if current_group.insert(c) {
                total += 1;
            }
        }
    }

    println!("Total is: {}", total);

    Ok(())
}
