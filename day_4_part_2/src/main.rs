extern crate pest;
#[macro_use]
extern crate pest_derive;

use pest::Parser;

#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct PassportParser;

fn main() -> std::io::Result<()> {
    let data = std::fs::read_to_string("./day_4_part_2/input")?;

    let parsed = PassportParser::parse(Rule::passports, &data).unwrap();
    let mut passports = vec![];

    let mut valid = 0;
    for passport in parsed {
        let passport = Passport::from_pair(passport);

        passports.push(passport);
    }

    for passport in passports.iter() {
        if passport.is_valid() {
            valid += 1;
        }
    }

    println!("{} valid passports.", valid);

    Ok(())
}

#[derive(Debug)]
struct Passport<'a> {
    pub byr: Option<i32>,
    pub iyr: Option<i32>,
    pub eyr: Option<i32>,
    pub hgt: Option<&'a str>,
    pub hcl: Option<&'a str>,
    pub ecl: Option<&'a str>,
    pub pid: Option<&'a str>,
    pub cid: Option<&'a str>,
}

impl Passport<'_> {
    pub fn from_pair(pair: pest::iterators::Pair<Rule>) -> Passport {
        let passport = match pair.as_rule() {
            Rule::passport => pair.into_inner(),
            _ => unreachable!(),
        };

        let mut result = Passport {
            byr: None,
            iyr: None,
            eyr: None,
            hgt: None,
            hcl: None,
            ecl: None,
            pid: None,
            cid: None,
        };

        for property in passport {
            let (property_type, value) = Passport::get_property(property);

            match property_type {
                "byr" => result.byr = value.parse::<i32>().ok(),
                "iyr" => result.iyr = value.parse::<i32>().ok(),
                "eyr" => result.eyr = value.parse::<i32>().ok(),
                "hgt" => result.hgt = Some(value),
                "hcl" => result.hcl = Some(value),
                "ecl" => result.ecl = Some(value),
                "pid" => result.pid = Some(value),
                "cid" => result.cid = Some(value),
                _ => unimplemented!(),
            }
        }

        result
    }

    pub fn get_property<'a>(pair: pest::iterators::Pair<'a, Rule>) -> (&'a str, &'a str) {
        let mut inner = pair.into_inner();
        let property_type = inner.nth(0).unwrap();
        let property_value = inner.nth(0).unwrap();

        (property_type.as_str(), property_value.as_str())
    }

    pub fn is_valid(&self) -> bool {
        match self.byr.unwrap_or_default() {
            1920..=2002 => {}
            _ => return false,
        };

        match self.iyr.unwrap_or_default() {
            2010..=2020 => {}
            _ => return false,
        };

        match self.eyr.unwrap_or_default() {
            2020..=2030 => {}
            _ => return false,
        };

        if self.hgt.is_none() {
            return false;
        }

        let hgt = self.hgt.unwrap();
        let height = hgt[..hgt.len() - 2].parse::<i32>().unwrap_or_default();
        if hgt.contains("cm") {
            if height < 150 || height > 193 {
                return false;
            }
        } else if hgt.contains("in") {
            if height < 59 || height > 76 {
                return false;
            }
        } else {
            return false;
        }

        let hcl = self.hcl.unwrap_or_default();
        if hcl.len() != 7 || &hcl[0..1] != "#" || hcl[1..].chars().any(|c| !c.is_ascii_hexdigit()) {
            return false;
        }

        match self.ecl.unwrap_or_default() {
            "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth" => {},
            _ => return false,
        };

        if self.pid.unwrap_or_default().chars().any(|c| !c.is_ascii_digit()) || self.pid.unwrap_or_default().len() != 9 {
            return false;
        }

        true
    }
}
