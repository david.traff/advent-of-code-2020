extern crate pest;
#[macro_use]
extern crate pest_derive;

use pest::Parser;

#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct PassportParser;

fn main() -> std::io::Result<()> {
    let data = std::fs::read_to_string("./day_4_part_1/input")?;

    let parsed = PassportParser::parse(Rule::passports, &data).unwrap();

    let mut valid = 0;
    for passport in parsed {
        let passport = Passport::from_pair(passport);

        if passport.is_valid() {
            valid += 1;
        }
    }

    println!("{} valid passports.", valid);

    Ok(())
}

#[derive(Debug)]
struct Passport<'a> {
    pub byr: Option<&'a str>,
    pub iyr: Option<&'a str>,
    pub eyr: Option<&'a str>,
    pub hgt: Option<&'a str>,
    pub hcl: Option<&'a str>,
    pub ecl: Option<&'a str>,
    pub pid: Option<&'a str>,
    pub cid: Option<&'a str>,
}

impl Passport<'_> {
    pub fn from_pair(pair: pest::iterators::Pair<Rule>) -> Passport {
        let passport = match pair.as_rule() {
            Rule::passport => pair.into_inner(),
            _ => unreachable!()
        };

        let mut result = Passport {
            byr: None,
            iyr: None,
            eyr: None,
            hgt: None,
            hcl: None,
            ecl: None,
            pid: None,
            cid: None,
        };

        for property in passport {
            let (property_type, value) = Passport::get_property(property);

            match property_type {
                "byr" => result.byr = Some(value),
                "iyr" => result.iyr = Some(value),
                "eyr" => result.eyr = Some(value),
                "hgt" => result.hgt = Some(value),
                "hcl" => result.hcl = Some(value),
                "ecl" => result.ecl = Some(value),
                "pid" => result.pid = Some(value),
                "cid" => result.cid = Some(value),
                _ => unimplemented!(),
            }
        }

        result
    }

    pub fn get_property<'a>(pair: pest::iterators::Pair<'a, Rule>) -> (&'a str, &'a str) {
        let mut inner = pair.into_inner();
        let property_type = inner.nth(0).unwrap();
        let property_value = inner.nth(0).unwrap();

        (property_type.as_str(), property_value.as_str())
    }

    pub fn is_valid(&self) -> bool {
        self.byr.is_some() &&
        self.iyr.is_some() &&
        self.eyr.is_some() &&
        self.hgt.is_some() &&
        self.hcl.is_some() &&
        self.ecl.is_some() &&
        self.pid.is_some()
    }
}
