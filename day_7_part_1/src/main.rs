#![feature(str_split_once)]

use std::collections::HashMap;
use std::fs::File;
use std::io::{prelude::*, BufReader, Result};
use regex::Regex;

fn main() -> Result<()> {
    // muted lavender bags contain 5 dull brown bags, 4 pale maroon bags, 2 drab orange bags.
    let file = File::open("./day_7_part_1/input")?;
    let reader = BufReader::new(file);

    let child_regex = Regex::new(r"([0-9])\s(\w+\s\w+)").unwrap();

    let mut bags = HashMap::new();
    for line in reader.lines() {
        let line = line.unwrap();
        
        let (color, children) = line.split_once("bags contain").unwrap();
        let color = color.trim().to_string();

        bags.entry(color.clone()).or_insert(vec![]);

        for child in child_regex.captures_iter(children) {
            let amount = child[1].parse::<u32>().unwrap();
            let child_color = child[2].to_string();

            let child = bags.entry(child_color).or_insert(vec![]);

            child.push(Connection {
                name: (&color).clone(),
                amount,
            });
        }
    }
    
    traverse(&bags, String::from("shiny gold"));

    Ok(())
}

fn traverse(data: &HashMap<String, Vec<Connection>>, name: String) {
    let mut result = vec![];
    let connection = &Connection { name, amount: 1 };

    traverse_parents(data, connection, &mut result);

    result.sort_by(|a,b| a.name.cmp(&b.name));
    result.dedup_by(|a, b| a.name.eq(&b.name));

    println!("{}", result.len());
}

fn traverse_parents<'a>(data: &'a HashMap<String, Vec<Connection>>, connection: &'a Connection, result: &mut Vec<&'a Connection>) {
    let parents = data.get(&connection.name).unwrap();

    for parent in parents {
        result.push(parent);
        traverse_parents(data, parent, result);
    }
}

#[derive(Debug)]
struct Connection {
    pub name: String,
    pub amount: u32,
}
