use std::fs::File;
use std::io::{prelude::*, BufReader, Result};

fn main() -> Result<()>  {
    let file = File::open("./day_1_part_1/input")?;
    let reader = BufReader::new(file);

    let expenses: Vec<u32> = reader
        .lines()
        .map(|x| x.unwrap().parse::<u32>().unwrap())
        .collect();

    'outer: for first in expenses.iter() {
        for second in expenses.iter() {
            for third in expenses.iter() {
                if first + second + third == 2020 {
                    println!("Answer: {}", first * second * third);
                    break 'outer;
                }
            }
        }
    }

    Ok(())
}
