#![feature(str_split_once)]

use std::{collections::HashSet, fs::File};
use std::io::{prelude::*, BufReader, Result};

fn main() -> Result<()> {
    let mut cpu = CPU::new("./day_8_part_1/input")?;

    let mut executed = HashSet::new();
    loop {
        cpu.cycle();

        if !executed.insert(cpu.ip) {
            break;
        }
    }

    println!("{}", cpu.acc);

    Ok(())
}

struct CPU {
    acc: i32,
    ip: i32,
    program: Vec<Instruction>
}

impl CPU {
    pub fn new(program_path: &str) -> Result<CPU> {
        let file = File::open(program_path)?;
        let program = BufReader::new(file).lines().map(|x| x.unwrap().into()).collect();

        let cpu = CPU {
            acc: 0,
            ip: 0,
            program
        };

        Ok(cpu)
    }

    pub fn cycle(&mut self) {
        let instruction = &self.program[self.ip as usize];

        match instruction {
            Instruction::NOP => self.ip += 1,
            Instruction::JMP(offset) => self.ip += *offset,
            Instruction::ACC(arg) => {
                self.acc += *arg;
                self.ip += 1;
            },
        }
    }
}

enum Instruction {
    NOP,
    ACC(i32),
    JMP(i32)
}

impl From<String> for Instruction {
    fn from(instruction: String) -> Self {
        let (instruction, argument) = instruction.split_once(" ").unwrap();
        let argument = argument.parse::<i32>().unwrap();
    
        match instruction {
            "nop" => Instruction::NOP,
            "jmp" => Instruction::JMP(argument),
            "acc" => Instruction::ACC(argument),
            _ => unimplemented!(),
        }
    }
}
