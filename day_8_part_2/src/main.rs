#![feature(str_split_once)]

use std::io::{prelude::*, BufReader, Result};
use std::{collections::HashSet, fs::File};

fn main() -> Result<()> {
    let mut offset = 0usize;
    'outer: loop {
        let mut cpu = CPU::new("./day_8_part_2/input")?;

        for i in offset..cpu.program.len() {
            match &cpu.program[i] {
                Instruction::NOP(arg) => {
                    if *arg != 0 {
                        cpu.program[i] = Instruction::JMP(*arg);
                        offset = i + 1;
                        break;
                    }
                },
                Instruction::JMP(arg) => {
                    cpu.program[i] = Instruction::NOP(*arg);
                    offset = i + 1;
                    break;
                },
                _ => {},
            }
        }

        let mut executed = HashSet::new();
        loop {
            if cpu.cycle() {
                println!("The program terminated: ACC={}", cpu.acc);
                break 'outer;
            }

            if !executed.insert(cpu.ip) {
                break;
            }
        }
    }

    Ok(())
}

struct CPU {
    acc: i32,
    ip: i32,
    program: Vec<Instruction>,
}

impl CPU {
    pub fn new(program_path: &str) -> Result<CPU> {
        let file = File::open(program_path)?;
        let program = BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().into())
            .collect();

        let cpu = CPU {
            acc: 0,
            ip: 0,
            program,
        };

        Ok(cpu)
    }

    pub fn cycle(&mut self) -> bool {
        let instruction = &self.program[self.ip as usize];

        match instruction {
            Instruction::NOP(_) => self.ip += 1,
            Instruction::JMP(offset) => self.ip += *offset,
            Instruction::ACC(arg) => {
                self.acc += *arg;
                self.ip += 1;
            }
        }

        self.ip == self.program.len() as i32
    }
}

enum Instruction {
    NOP(i32),
    ACC(i32),
    JMP(i32),
}

impl From<String> for Instruction {
    fn from(instruction: String) -> Self {
        let (instruction, argument) = instruction.split_once(" ").unwrap();
        let argument = argument.parse::<i32>().unwrap();

        match instruction {
            "nop" => Instruction::NOP(argument),
            "jmp" => Instruction::JMP(argument),
            "acc" => Instruction::ACC(argument),
            _ => unimplemented!(),
        }
    }
}
