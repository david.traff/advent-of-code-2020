use std::fs::File;
use std::io::{prelude::*, BufReader, Result};

fn main() -> Result<()> {
    let slope = Slope { right: 3, down: 1 };
    let map = Map::new("./day_3_part_1/input")?;

    let mut trees = 0;
    for i in 0..map.height {
        if *map.get_point_at_step(i, &slope) {
            trees += 1;
        }
    }

    println!("Trees: {}", trees);

    Ok(())
}

struct Slope {
    down: usize,
    right: usize,
}

struct Map {
    data: Vec<bool>,
    height: usize, // Height of data.
    width: usize,  // Stride of data.
}

impl Map {
    pub fn new(path: &str) -> Result<Map> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);
        let mut data = vec![];

        let mut width = 0usize;
        let mut height = 0usize;
        for line in reader.lines() {
            let line = line.unwrap();
            if width == 0 {
                width = line.len();
            }

            height += 1;

            for c in line.chars() {
                let tree = match c {
                    '#' => true,
                    '.' => false,
                    _ => panic!("Error parsing data."),
                };

                data.push(tree);
            }
        }

        Ok(Map {
            data,
            width,
            height,
        })
    }

    pub fn get_point_at_step(&self, step: usize, slope: &Slope) -> &bool {
        let x = (slope.right * step) % self.width;
        let y = slope.down * step;

        let idx = x + (y * self.width);

        &self.data[idx]
    }
}
